# Comporre ai Confini

"Comporre ai Confini" is a lecture on compositional methods for contemporary music (in italian, at the present time)

## Compiled version of the slides

A compiled version of the slides may be found over to
[SlideShare](http://www.slideshare.net/NicolaBernardini2/comporre-ai-confini)

## Compilation Instructions

In order to compile the slides you need to also clone the [LaTeX
templates](https://github.com/nicb/latex-templates) at the same level of the
this top root directory.

## Presentation log

* Festival *Confini Mediterranei*, Conservatorio "G.Martucci" Salerno, October 21 2015
