# Commporre ai Confini (notes)


* *Comporre ai confini* vs. *Comporre al Confine*: differences
* Functions of music
* Why compositional spculation means expanding boundaries:
  * Solve problems you do not know how to solve
  * Write things that you do not know how to write
  * Learn instruments and tools that (you hope) are right to solve the issues you want to solve
  * Build tools and instruments that will solve the remaining technological boundaries
* Some case studies from other composers:
  * the compositional trajectory of Giacinto Scelsi
  * Luciano Berio *Ofanim*
  * Luciano Berio *Outis*
  * the manuscript scores of Adriano Guarnieri
* Some case studies from my own production:
  * *Partita*
  * *Studi*
  * *Intermezzo I*
  * *Recordare*
* Conclusions
  * unsolved problems
  * unsolvable problems
  * how to live with it
