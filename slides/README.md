# Comporre ai Confini

## Compilation Instructions

In order to compile the slides you need to also clone the [LaTeX
templates](https://github.com/nicb/latex-templates) at the same level of the
this top root directory.
